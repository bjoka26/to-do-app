import React from 'react';
import { useState, useEffect } from 'react';
import axios from 'axios';

const useDatabase = () => {
    const [items, setItems] = useState([]);

    const addItems = async (item) => {
        try {
            await axios.post("http://localhost:3000/notes", item);
        } catch (error) {
            console.log("error: " + error);
        }
        await getItems();
    }

    const getItems = async () => {
        try {
            const items = await axios.get("http://localhost:3000/notes");

            setItems(items.data);

        } catch (error) {
            console.log("error: " + error);
        }
    }

    const deleteItems = (deletedItem) => {
        const deletingItem = items.filter((item) => item.id !== deletedItem);

        try {
            axios.delete(`http://localhost:3000/notes/${deletedItem}`);
        } catch (error) {
            console.log("error: " + error);
        }

        setItems(deletingItem);
    }

    useEffect(() => {
      getItems();
    }, [])
    
    return { addItems, items, deleteItems }
}

export default useDatabase;