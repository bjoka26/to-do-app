import React, { useState } from 'react';
import { FaTrash } from 'react-icons/fa';

function Note({ item, deleteItems, handleModal }) {
    const [deleteAnim, setDeleteAnim] = useState(false);

    const handleDelete = () => {
        setDeleteAnim(true);
        setTimeout(() => {
            deleteItems(item.id);
            setDeleteAnim(false);
        }, 1000)
    }

    return (
        <div className={deleteAnim ? `task-box deletedItem` : 'task-box'} onClick={handleModal}>
            <div className='hovering'></div>
            <h4>{item.title}</h4>
            <p>{item.note}</p>
            <div className='delete-btn' onClick={handleDelete}>
                <i><FaTrash /></i>
            </div>
        </div>
    );
}

export default Note;