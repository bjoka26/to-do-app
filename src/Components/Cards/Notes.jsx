import React, { useState } from 'react';
import Note from './Note';

function Notes({ items, deleteItems }) {  
  
  return (
    <div className='tasks-container'>
      {items.length > 0 ? items?.map((item, id) =>  {
          return <Note item={item} key={id} deleteItems={deleteItems} />
        })
        : 
        <div className='tasks-container'>
          <p className='no-tasks'>No notes yet</p>
        </div>
      }
    </div>
  );
}

export default Notes;