import React from 'react';

const Notification = ({ notification }) => {
  return (
    <div className={notification.type === 'error' ? 'notification notify-error' : 'notification notify-success'}>
      <p>{notification.message}</p>
    </div>
  );
}

export default Notification;