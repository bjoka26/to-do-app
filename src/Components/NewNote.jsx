import React from 'react';
import { useState } from 'react';
import Notification from './Notification';

function NewNote({ addItems }) {
  const [showNotification, setShowNotification] = useState(false);
  const [notification, setNotification] = useState({
    type: '',
    message: ''
  })

  const [item, setItem] = useState({
    id: 0,
    title: '',
    note: ''
  });

  const createNotification = (type, msg) => {
    setShowNotification(true);
    setNotification({
      type: type,
      message: msg
    });

    setTimeout(() => {
      setShowNotification(false);
    }, 2500);

  }

  const handleInput = (e) => {
    const newItem = {...item};
    newItem[e.target.id] = e.target.value;
    setItem(newItem);
  }

  const addCard = (e) => {
    e.preventDefault();

    if (item.title === '' && item.note === '') {
      createNotification("error", "Both fields can't be empty");

      return;
    }

    addItems(item);
    createNotification("success", "Note added successfully");
    setItem({
      id: 0,
      title: '',
      note: ''
    });
  }
  
  return (
    <form className='add-task'>
        <input className='task-title' type='text' placeholder='Title' id='title' value={item.title} onChange={(e) => handleInput(e)} />
        <textarea className='task-note' type='text' placeholder='Title' id='note' value={item.note} onChange={(e) => handleInput(e)} />
        <button className='add-btn' id='btn-new' onClick={addCard}>+</button>
        {showNotification && <Notification notification={notification} /> }
    </form>
  );
}

export default NewNote;