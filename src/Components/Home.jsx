import React from 'react';
import Navbar from './Navigation/Navbar';
import NewNote from './NewNote';
import Notes from './Cards/Notes';
import useDatabase from '../hooks/useDatabase';

function Home() {
  const { items, addItems, deleteItems } = useDatabase();

  return (
    <div>  
        <Navbar />
        <NewNote items={items} addItems={addItems} />
        <Notes items={items} deleteItems={deleteItems} />
    </div>
  );
}

export default Home;