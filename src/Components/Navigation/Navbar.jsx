import React from 'react';
import { FaReact } from "react-icons/fa";

function Navbar() {
  return (
    <div className='header'>
        <FaReact />
    </div>
  );
}

export default Navbar;